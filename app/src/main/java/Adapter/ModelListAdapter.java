package Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.example.andrefa.automibiapp.R;

import java.util.List;

import entity.Model;

/**
 * Created by andrefa on 29/01/18.
 */

public class ModelListAdapter extends RecyclerView.Adapter<ModelListAdapter.ViewHolder> {


    public static List<Model> listModel;
     public static Activity ac;

    public static class ViewHolder extends RecyclerView.ViewHolder {


        public TextView model;
        public TextView brand;
        public TextView version;
        public TextView modelYear;


        public ViewHolder(View v) {
            super(v);

            model  = (TextView)v.findViewById(R.id.idModel);
            brand  = (TextView)v.findViewById(R.id.idBrand);
            version  = (TextView)v.findViewById(R.id.idVersion);
            modelYear  = (TextView)v.findViewById(R.id.idModelYear);

        }


    }

    public ModelListAdapter(List<Model> list) {
        this.listModel = list;

    }

    @Override
    public ModelListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_model, parent, false);

        ModelListAdapter.ViewHolder vh = new ModelListAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Model mod = this.listModel.get(position);
        holder.model.setText(mod.getModel());
        holder.brand.setText(mod.getBrand());
        holder.version.setText(mod.getVersion());
        holder.modelYear.setText(String.valueOf(mod.getYear()));

    }

    @Override
    public int getItemCount() {
        return listModel.size();
    }


}
