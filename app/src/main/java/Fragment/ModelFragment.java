package Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.andrefa.automibiapp.R;

import java.util.List;

import Adapter.ModelListAdapter;
import ApiRest.ApiRest;
import entity.Model;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ModelFragment extends Fragment {



    private RecyclerView myRecyclerView;
    private RecyclerView.Adapter myAdapter;
    private RecyclerView.LayoutManager myLayoutManager;

    ApiRest apiRest;
    List<Model> listModels;

    public ModelFragment() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_model, container, false);

        myRecyclerView = (RecyclerView) view.findViewById(R.id.id_recycler_view_list_model);
        myRecyclerView.setHasFixedSize(true);

        apiRest = ApiRest.retrofit.create(ApiRest.class);


        Call<List<Model>> call = apiRest.getModels();

        call.enqueue(new Callback<List<Model>>() {
            @Override
            public void onResponse(Call<List<Model>> call, Response<List<Model>> response) {
                Log.d("MEU BODY === ",response.toString());
                listModels =  response.body();
                if(response.body().size() == 0){

                }
                else {

                    // use a linear layout manager
                    myLayoutManager = new LinearLayoutManager(getActivity());
                    myRecyclerView.setLayoutManager(myLayoutManager);


                    // specify an adapter (see also next example)
                    myAdapter = new ModelListAdapter(listModels);
                    myRecyclerView.setAdapter(myAdapter);

                    RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
                    myRecyclerView.addItemDecoration(itemDecoration);


                }
            }

            @Override
            public void onFailure(Call<List<Model>> call, Throwable t) {
                Log.d("MINHA FALHAR == ", t.toString());
                Toast.makeText(getContext(), " falha em buscar dados ", Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }

}
