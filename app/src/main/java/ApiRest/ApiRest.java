package ApiRest;

import java.util.List;

import entity.Model;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by andrefa on 29/01/18.
 */

public interface ApiRest {

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://192.168.0.25:3000/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @GET("models")
    Call<List<Model>> getModels();


}
